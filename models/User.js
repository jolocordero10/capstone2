const mongoose = require('mongoose');

const user_schema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Email is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderedProduct: [{
		products: {
        	productId: {
            	type: mongoose.Schema.Types.ObjectId,
            	ref: 'Product'
        	},
        	productName: {
        		type: String
        	},
        	quantity: {
            	type: Number,
            	default: 1
        	}
    	},
    	totalAmount: {
        	type: Number
        },
    	purchasedOn: {
        	type: Date,
        	default: Date.now
    	}
	}]
})

module.exports = mongoose.model('User', user_schema);