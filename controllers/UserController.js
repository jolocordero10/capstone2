const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const Product = require('../models/Product.js');

// User Registration
module.exports.registerUser = (request_body) => {
	return User.findOne({email: request_body.email}).then(result => {
		if(result == null){
			let new_user = new User({
				email: request_body.email,
				password: bcrypt.hashSync(request_body.password, 10)
			});

			return new_user.save().then((registered_user, error) => {
				if(error){
					return {
						message: error.message
					};
				}
		
				return {
					message: 'Successfully registered a user!'
				};
			})			
		} else {
			return {
				message: 'User already exist.'
			}
		}
	}).catch(error => console.log(error));
}


// User Login
module.exports.loginUser = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {
		if(result == null){
			return response.send({
				message: "The user isn't registered yet."
			}) 
		}
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Your password is incorrect.'
			})
		}
	}).catch(error => response.send(error));
}

// Non-admin User checkout
/*module.exports.checkout = async (request, response) => {
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	let isUserUpdated = await User.findById(request.user.id).then(user => {
		let quantity = request.body.quantity
		let product = Product.findById(request.body.productId).then(product => {
			let new_checkout = {
					products: {
        				productId: product._id,
        				productName: product.name,
        				quantity: quantity,
    				},
    			totalAmount: quantity * product.price,
    			purchasedOn: new Date()
    	};

    	if (quantity <= 0) {
      	return response.send('Invalid quantity');
    	} else {
    			user.orderedProduct.push(new_checkout);
    			return user.save().then(updated_user => true).catch(error => error.message);
    	}
		});
	});

	let isProductUpdated = await Product.findById(request.body.productId).then(product => {
		let new_order = {
			userId : request.user.id
		};
		product.userOrders.push(new_order);
		return product.save().then(updated_product => true).catch(error => error.message)
	});

	if (isUserUpdated !== true) {
    return response.send({ message: isUserUpdated });
  }

  if (isProductUpdated !== true) {
    return response.send({ message: isProductUpdated });
  }
};*/

// Retrieve User Details
module.exports.getProfile = (request, response) => {
	return User.findById(request.user.id).then((user, error) => {
		if(error){
			return {
				message: error.message 
			}
		}
		user.password = "";
		return response.send(user);
	}).catch(error => console.log(error));
}


// Check Existing User
module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then(result => {
		
		if(result.length > 0) {
			return true;
		} else {
			return false;
		}
	})
};

// Non-admin User checkout v2
module.exports.checkout = async (request, response) => {
  if (request.user.isAdmin) {
    return response.send('Action Forbidden');
  }

  let isUserUpdated;
  let isProductUpdated;

  let user = await User.findById(request.user.id);
  let product = await Product.findById(request.body.productId);

  if (!product) {
    return response.send('Product not found');
  }

  let quantity = request.body.quantity;
  let price = product.price;

  if (quantity <= 0) {
    return response.send('Invalid quantity');
  }

  let totalPrice = quantity * price;

  let new_checkout = {
    	products: {
      	productId: request.body.productId,
      	productName: product.name,
      	quantity: quantity,
    	},
    	totalAmount: totalPrice,
    	purchasedOn: new Date(),
  	};

  let new_order = {
			userId : request.user.id
		};

  user.orderedProduct.push(new_checkout);
  product.userOrders.push(new_order);

  isUserUpdated = await user.save();
  isProductUpdated = await product.save();

  if (isUserUpdated && isProductUpdated) {
    return response.send(true);
  } else {
    return response.send(false);
  }
};