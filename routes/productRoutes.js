const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');

// Create Product
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addProduct(request, response)
});

// Retrieve all products
router.get('/all', (request, response) => {
	ProductController.getAllProducts(request, response);
});

// Retrieve all active products
router.get('/active', (request, response) => {
	ProductController.getAllActiveProducts(request, response);
})

// Retrieve single product
router.get('/:id', (request, response) => {
	ProductController.getProduct(request, response);
})

// Update Product information
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
})

// Archive Product
router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.archiveProduct(request, response);
})

// Activate Product
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response);
})

module.exports = router;